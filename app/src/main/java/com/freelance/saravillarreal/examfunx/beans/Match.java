package com.freelance.saravillarreal.examfunx.beans;

import java.io.Serializable;

public class Match implements Serializable {
    private String uuid;
    private LocalTeam local_team;
    private VisitTeam visit_team;
    private String referee;
    private int local_goals;
    private int visit_goals;
    private String date;
    private String time;
    private int minute;
    private String image_streaming;
    private String alineaciones;
    private String heatmap;
    private Stadium stadium;
    private int status;
    private int id_opta;
    private String slug;
    private String stage;
    private Competition competition;
    private Double timestamp;
    private Double weather;
    private String city_reference_a;
    private String city_reference_b;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public LocalTeam getLocal_team() {
        return local_team;
    }

    public void setLocal_team(LocalTeam local_team) {
        this.local_team = local_team;
    }

    public VisitTeam getVisit_team() {
        return visit_team;
    }

    public void setVisit_team(VisitTeam visit_team) {
        this.visit_team = visit_team;
    }

    public String getReferee() {
        return referee;
    }

    public void setReferee(String referee) {
        this.referee = referee;
    }

    public int getLocal_goals() {
        return local_goals;
    }

    public void setLocal_goals(int local_goals) {
        this.local_goals = local_goals;
    }

    public int getVisit_goals() {
        return visit_goals;
    }

    public void setVisit_goals(int visit_goals) {
        this.visit_goals = visit_goals;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public String getImage_streaming() {
        return image_streaming;
    }

    public void setImage_streaming(String image_streaming) {
        this.image_streaming = image_streaming;
    }

    public String getAlineaciones() {
        return alineaciones;
    }

    public void setAlineaciones(String alineaciones) {
        this.alineaciones = alineaciones;
    }

    public String getHeatmap() {
        return heatmap;
    }

    public void setHeatmap(String heatmap) {
        this.heatmap = heatmap;
    }

    public Stadium getStadium() {
        return stadium;
    }

    public void setStadium(Stadium stadium) {
        this.stadium = stadium;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getId_opta() {
        return id_opta;
    }

    public void setId_opta(int id_opta) {
        this.id_opta = id_opta;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public Competition getCompetition() {
        return competition;
    }

    public void setCompetition(Competition competition) {
        this.competition = competition;
    }

    public Double getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Double timestamp) {
        this.timestamp = timestamp;
    }

    public Double getWeather() {
        return weather;
    }

    public void setWeather(Double weather) {
        this.weather = weather;
    }

    public String getCity_reference_a() {
        return city_reference_a;
    }

    public void setCity_reference_a(String city_reference_a) {
        this.city_reference_a = city_reference_a;
    }

    public String getCity_reference_b() {
        return city_reference_b;
    }

    public void setCity_reference_b(String city_reference_b) {
        this.city_reference_b = city_reference_b;
    }
}
