package com.freelance.saravillarreal.examfunx.interactors;

import android.util.Log;

import com.freelance.saravillarreal.examfunx.beans.APIResponse;
import com.freelance.saravillarreal.examfunx.beans.Match;
import com.freelance.saravillarreal.examfunx.interfaces.MatchInteractorInterface;
import com.freelance.saravillarreal.examfunx.interfaces.MatchOnListener;
import com.freelance.saravillarreal.examfunx.services.ApiService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

public class MatchInteractorImpl implements MatchInteractorInterface {

    public static ApiService apiService;

    @Override
    public void callMatchsService(final MatchOnListener listener) {

        // callService
        Log.i("callMatchsService", "callMatchsService");
        //get unique Instance of ApiService
        apiService = apiService.newInstance();


        Call<APIResponse> catalogueService =  apiService.getMatchesFromCompetition("torneo-descentralizado");
        catalogueService.enqueue(new retrofit2.Callback<APIResponse>() {

            @Override
            public void onResponse(Call<APIResponse> call, Response<APIResponse> response) {
                switch (response.code()) {
                    case 200:
                        try{
                            Gson gson = new Gson();
                            //Convert response.body().getMatchs() to JsonArray because coming in LinkedTreeMap
                            JsonArray jsonArray = gson.toJsonTree(response.body().getMatchs()).getAsJsonArray();
                            Type type = new TypeToken<List<Match>>() {}.getType();
                            //Transform the JsonArray to List <Match>
                            List<Match> matchList = gson.fromJson(jsonArray, type);
                            listener.resultMatchs(matchList);

                        }
                        catch (Exception e){
                            String exs = e.toString();
                            String stuatus = "NONE";
                        }

                        break;
                    case 401:

                        break;
                    default:

                        break;
                }
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                Log.i("onFailure", "loadMatch");
                listener.resultMatchError();
            }
        });
    }
}
