package com.freelance.saravillarreal.examfunx.globals;


import com.freelance.saravillarreal.examfunx.services.ApiService;
import com.freelance.saravillarreal.examfunx.services.RetrofitClient;

/**
 * Created by Sara on 02-01-2018.
 */

public class GlobalUrl {

    private GlobalUrl() {}


    /***** Developer *******/

    public static final String BASE_URL = "http://sporting.backend.masfanatico.cl/api/";



    public static ApiService getAPIService() {
        return RetrofitClient.getClient(BASE_URL).create(ApiService.class);
    }
}
