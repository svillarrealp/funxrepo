package com.freelance.saravillarreal.examfunx.beans;

import java.io.Serializable;

public class VisitTeam implements Serializable {
    private String uuid;
    private String name;
    private String image;
    private String color_scoreboard;
    private String slug;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getColor_scoreboard() {
        return color_scoreboard;
    }

    public void setColor_scoreboard(String color_scoreboard) {
        this.color_scoreboard = color_scoreboard;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
