package com.freelance.saravillarreal.examfunx.interfaces;

import com.freelance.saravillarreal.examfunx.beans.Match;

import java.util.List;

public interface MatchViewInterface {

    void showMatches(List<Match> matchList);
    void showErrorService();
}
