package com.freelance.saravillarreal.examfunx.presenters;

import com.freelance.saravillarreal.examfunx.beans.Match;
import com.freelance.saravillarreal.examfunx.interactors.MatchInteractorImpl;
import com.freelance.saravillarreal.examfunx.interfaces.MatchInteractorInterface;
import com.freelance.saravillarreal.examfunx.interfaces.MatchOnListener;
import com.freelance.saravillarreal.examfunx.interfaces.MatchPresenterInterface;
import com.freelance.saravillarreal.examfunx.interfaces.MatchViewInterface;

import java.util.List;

public class MatchPresenterImpl implements MatchPresenterInterface, MatchOnListener{

    private MatchViewInterface view;
    private MatchInteractorInterface interactor;


    public MatchPresenterImpl(MatchViewInterface view) {
        this.view = view;
        this.interactor = new MatchInteractorImpl();
    }

    @Override
    public void callMatchs() {
        interactor.callMatchsService(this);
    }

    @Override
    public void resultMatchs(List<Match> matchList) {
        view.showMatches(matchList);
    }

    @Override
    public void resultMatchError() {
        view.showErrorService();
    }
}
