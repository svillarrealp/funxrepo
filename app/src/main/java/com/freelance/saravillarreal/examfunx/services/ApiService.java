package com.freelance.saravillarreal.examfunx.services;

import com.freelance.saravillarreal.examfunx.beans.APIResponse;
import com.freelance.saravillarreal.examfunx.globals.GlobalUrl;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Sara on 02-01-2018.
 */

public class ApiService {
    private static ApiService apiInstance;
    private Api mApi;

    /**
     * Private constructor
     */
    private ApiService() {
        Gson gson = new GsonBuilder()
                .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
        String baseUrl;

        baseUrl = GlobalUrl.BASE_URL;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        mApi = retrofit.create(Api.class);
    }

    public static ApiService newInstance() {

        ApiService fragment = new ApiService();
        return fragment;
    }


    /**
     * Singlenton Pattern
     *
     * @return ApiService {@link ApiService}
     */
    public static final synchronized ApiService getInstance() {
        if (apiInstance == null) {
            apiInstance = new ApiService();
        }
        return apiInstance;
    }

    /**
     * Getting the matches from competition
     *
     */
    public Call<APIResponse> getMatchesFromCompetition(String competition) {
        return mApi.getMatchescompetition(competition);
    }




}
