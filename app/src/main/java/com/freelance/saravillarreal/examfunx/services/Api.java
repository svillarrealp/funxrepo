package com.freelance.saravillarreal.examfunx.services;


import com.freelance.saravillarreal.examfunx.beans.APIResponse;

import retrofit2.Call;
import retrofit2.http.GET;

import retrofit2.http.Query;


/**
 * Created by Sara on 02-01-2018.
 */

public interface Api {

    @GET("match/in_competition/")
    Call<APIResponse> getMatchescompetition(@Query("competition") String competition);



}
