package com.freelance.saravillarreal.examfunx.adapters;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.CalendarContract;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.freelance.saravillarreal.examfunx.R;
import com.freelance.saravillarreal.examfunx.beans.Match;
import com.freelance.saravillarreal.examfunx.util.DateUtil;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.MatchViewHolder> {

    public final static String TAG = "MatchAdapter";
    Activity activity;
    List<Match> matchList;
    public static final Integer DEFAULT_CALENDAR_ID = 1;

    public MatchAdapter (Activity activity, List<Match> matchList){
        this.activity = activity;
        this.matchList = matchList;
    }


    @Override
    public MatchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_matches,parent,false);
        MatchViewHolder carouselViewHolder = new MatchViewHolder(view);
        return carouselViewHolder;
    }

    @Override
    public void onBindViewHolder(MatchViewHolder holder, final int position) {


        Picasso.with(activity)
                .load(matchList.get(position).getLocal_team().getImage())
                .into(holder.ivLocal);

        Picasso.with(activity)
                .load(matchList.get(position).getVisit_team().getImage())
                .into(holder.ivVisitor);
        holder.txtLocal.setText(matchList.get(position).getLocal_team().getName());
        holder.txtVisitor.setText(matchList.get(position).getVisit_team().getName());
        holder.txtScoreLocal.setText(String.valueOf(matchList.get(position).getLocal_goals()));
        holder.txtScoreVisitor.setText(String.valueOf(matchList.get(position).getVisit_goals()));
        holder.txtDate.setText(DateUtil.getFormattedDateMatch(matchList.get(position).getDate()));
        holder.txtHours.setText(DateUtil.getFormattedHourMatch(matchList.get(position).getTime()));
        holder.txtStadium.setText(matchList.get(position).getStadium().getName());

        holder.ivCalendar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(activity, R.string.calendar_set, Toast.LENGTH_SHORT).show();
                Calendar dateCalendar =  DateUtil.putDateCalendar(matchList.get(position).getDate(),activity.getResources().getConfiguration().locale.getLanguage() );
                addAlarm(dateCalendar,matchList.get(position).getCompetition().getName(),
                        matchList.get(position).getLocal_team().getName() + " vs. " +
                                matchList.get(position).getVisit_team().getName(),
                        matchList.get(position).getTime());

            }
        });

    }

    @Override
    public int getItemCount() {
        return matchList.size();
    }

    private void addAlarm(Calendar calendar, String title, String descriptionCalendar, String hourMatch  ) {
        ContentResolver contentResolver = null;
        String mDescription = "";
        try {
            mDescription = new String(descriptionCalendar.getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        Calendar initDate = Calendar.getInstance();
        Calendar entDate = Calendar.getInstance();
        initDate.setTimeInMillis(calendar.getTimeInMillis());
        entDate.setTimeInMillis(calendar.getTimeInMillis()+ 60 * 60 * 1000);
        contentResolver = activity.getContentResolver();
        ContentValues appointmentMatches = new ContentValues();
        appointmentMatches.put(CalendarContract.Events.DTSTART, initDate.getTimeInMillis());
        appointmentMatches.put(CalendarContract.Events.DTEND, entDate.getTimeInMillis());
        appointmentMatches.put(CalendarContract.Events.TITLE, title);
        appointmentMatches.put(CalendarContract.Events.DESCRIPTION, mDescription);
        appointmentMatches.put(CalendarContract.Events.HAS_ALARM, "1");
        appointmentMatches.put(CalendarContract.Events.CALENDAR_ID, DEFAULT_CALENDAR_ID);
        appointmentMatches.put(CalendarContract.Events.EVENT_TIMEZONE, hourMatch);
        Uri createdEventInCalendar = null;
        Long eventStoredId = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (activity.checkSelfPermission(Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
                createdEventInCalendar = contentResolver.insert(CalendarContract.Events.CONTENT_URI, appointmentMatches);
                eventStoredId = Long.parseLong(createdEventInCalendar.getLastPathSegment());
            } else {
                Log.i(TAG, "No permission granted for write on the system calendar");
            }
        } else {
            createdEventInCalendar = contentResolver.insert(CalendarContract.Events.CONTENT_URI, appointmentMatches);
            eventStoredId = Long.parseLong(createdEventInCalendar.getLastPathSegment());
        }
        if (eventStoredId != null) {
            //addAReminder(contentResolver, eventStoredId);
        }
        Log.i(TAG, "Se registro un evento en el calendario con id: " + eventStoredId);


    }








    public static class MatchViewHolder extends RecyclerView.ViewHolder{


        ImageView ivLocal, ivVisitor, ivCalendar;
        TextView txtLocal,txtDate, txtHours, txtStadium, txtVisitor,txtScoreLocal, txtScoreVisitor ;

        public MatchViewHolder(View itemView) {
            super(itemView);
            ivLocal = (ImageView) itemView.findViewById(R.id.iv_local);
            ivVisitor = (ImageView) itemView.findViewById(R.id.iv_visitor);
            ivCalendar = (ImageView) itemView.findViewById(R.id.iv_calendar);
            txtLocal = (TextView) itemView.findViewById(R.id.txt_name_local);
            txtDate = (TextView) itemView.findViewById(R.id.txt_date);
            txtHours = (TextView) itemView.findViewById(R.id.txt_hours);
            txtStadium = (TextView) itemView.findViewById(R.id.txt_stadium);
            txtVisitor = (TextView) itemView.findViewById(R.id.txt_name_visitor);
            txtScoreLocal = (TextView) itemView.findViewById(R.id.txt_score_local);
            txtScoreVisitor = (TextView) itemView.findViewById(R.id.txt_score_visitor);

        }
    }
}
