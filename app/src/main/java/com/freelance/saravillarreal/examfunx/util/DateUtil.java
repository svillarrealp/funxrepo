package com.freelance.saravillarreal.examfunx.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {


    public static String getFormattedDateMatch (String actualDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat month = new SimpleDateFormat("MMM");
        SimpleDateFormat day = new SimpleDateFormat("dd");
        String monthFormatted = "";
        String dayFormatted = "";
        Date d = null;
        try {
            d = sdf.parse(actualDate);
            monthFormatted = month.format(d);
            dayFormatted = day.format(d);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return dayFormatted + " de "+ monthFormatted;
    }


    public static String getFormattedHourMatch (String actualDate){
        //19:45:00
        SimpleDateFormat sdf = new SimpleDateFormat("HH:MM:ss");
        SimpleDateFormat hours = new SimpleDateFormat("HH");
        SimpleDateFormat minutes = new SimpleDateFormat("MM");
        String hoursFormatted;
        String minutesFormatted;
        String timeFormatted = "";
        Date d = null;
        try {
            d = sdf.parse(actualDate);
            hoursFormatted = hours.format(d);
            minutesFormatted = minutes.format(d);
            if (minutesFormatted != "00" ){
                timeFormatted = hoursFormatted + ":"+ minutesFormatted + "hrs";
            }
            else {
                timeFormatted = hoursFormatted+ "hrs";
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timeFormatted;
    }


    public static Calendar putDateCalendar(String dateAppointment, String lang){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat formatEs = new SimpleDateFormat("MM-dd-yyyy");
        SimpleDateFormat formatEn = new SimpleDateFormat("yyyy-MM-dd");

        if (lang.equalsIgnoreCase("es")){
            try {
                cal.setTime(formatEs.parse(dateAppointment));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        else{
            try {
                cal.setTime(formatEn.parse(dateAppointment));
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }


        return cal;
    }




}
