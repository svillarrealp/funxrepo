package com.freelance.saravillarreal.examfunx.beans;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Sara on 02-01-2018.
 */

public class APIResponse<T>  implements Serializable {

    private ArrayList<T> matchs;

    public ArrayList<T> getMatchs() {
        return matchs;
    }

    public void setMatchs(ArrayList<T> matchs) {
        this.matchs = matchs;
    }
}
