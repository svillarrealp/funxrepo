package com.freelance.saravillarreal.examfunx.beans;

import java.io.Serializable;

public class Stadium implements Serializable {

    private String uuid;
    private String name;
    private String image;
    private String image_scoreboard;
    private String slug;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage_scoreboard() {
        return image_scoreboard;
    }

    public void setImage_scoreboard(String image_scoreboard) {
        this.image_scoreboard = image_scoreboard;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }
}
