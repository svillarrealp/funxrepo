package com.freelance.saravillarreal.examfunx.views;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.freelance.saravillarreal.examfunx.R;
import com.freelance.saravillarreal.examfunx.activities.BaseActivity;
import com.freelance.saravillarreal.examfunx.adapters.MatchAdapter;
import com.freelance.saravillarreal.examfunx.beans.Match;
import com.freelance.saravillarreal.examfunx.interfaces.MatchPresenterInterface;
import com.freelance.saravillarreal.examfunx.interfaces.MatchViewInterface;
import com.freelance.saravillarreal.examfunx.presenters.MatchPresenterImpl;

import java.util.List;

public class MatchActivity extends BaseActivity implements MatchViewInterface {

    private MatchPresenterInterface presenter;
    RecyclerView recyclerView;
    MatchAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        presenter = new MatchPresenterImpl(this);
        showDialog();
        presenter.callMatchs();

    }


    @Override
    public void showMatches(List<Match> matchList) {
        dismissDialog();
        mAdapter = new MatchAdapter(this, matchList);
        recyclerView.setAdapter(mAdapter);
        RecyclerView.LayoutManager layoutManager =
                new LinearLayoutManager(MatchActivity.this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

    }

    @Override
    public void showErrorService() {
        dismissDialog();
        Toast.makeText(this, R.string.error_server, Toast.LENGTH_SHORT).show();
    }
}
