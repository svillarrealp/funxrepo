package com.freelance.saravillarreal.examfunx.interfaces;

import com.freelance.saravillarreal.examfunx.beans.Match;

import java.util.List;

public interface MatchOnListener {

    void resultMatchs(List<Match> matchList);
    void resultMatchError();

}
